﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Linq.Expressions;
using System.Reflection;

namespace FFactoryUI
{
    public partial class NotificationForm : Form
    {
        private bool showBorder;
        public bool ShowBorder
        {
            get { return showBorder; }
            set
            {
                showBorder = value;
                if (value)
                    CreateBottomBorder(panel1);
                else
                    CreateBottomBorder(panel1, !value);
            }
        }
        public PictureBox close
        {
            get { return this.imgClose; }
        }
        public Color BackGroundColor
        {
            get { return this.BackColor; }
            set { this.BackColor = value; this.textBox1.BackColor = value; }
        }

        public Image CloseImage
        {
            get { return imgClose.Image; }
            set { imgClose.Image = value; }
        }
        public bool CloseImageVisible
        {
            get { return this.imgClose.Visible; }
            set { this.imgClose.Visible = value; }
        }

        public Image Image
        {
            get { return img.Image; }
            set { img.Image = value; }
        }
        public bool ImageVisible
        {
            get { return this.img.Visible; }
            set { this.img.Visible = value; }
        }
        public Label Header
        {
            get { return this.lblTitle; }
        }

        public Font ContentFont
        {
            get { return this.textBox1.Font; }
            set { this.textBox1.Font = value; }
        }

        public Color ContentForeColor
        {
            get { return this.textBox1.ForeColor; }
            set { this.textBox1.ForeColor = value; }
        }

        public string ContentText
        {
            get { return this.textBox1.Text; }
            set
            {
                this.textBox1.Text = value;
            }
        }

        public HorizontalAlignment ContentTextAlign
        {
            get { return this.textBox1.TextAlign; }
            set { textBox1.TextAlign = value; }
        }

        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
        (
            int nLeftRect, // x-coordinate of upper-left corner
            int nTopRect, // y-coordinate of upper-left corner
            int nRightRect, // x-coordinate of lower-right corner
            int nBottomRect, // y-coordinate of lower-right corner
            int nWidthEllipse, // height of ellipse
            int nHeightEllipse // width of ellipse
        );

        public NotificationForm()
        {
            this.Visible = false;
            this.Opacity = 0;
            InitializeComponent();            
            this.FormBorderStyle = FormBorderStyle.None;
            Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, Width, Height, 20, 20));
            Rectangle workingArea = Screen.GetWorkingArea(this);
            this.Location = new Point(workingArea.Right - Size.Width,
                                      workingArea.Bottom - Size.Height);
        }

        private void CreateBottomBorder(Control c, bool remove = false)
        {
            if (!remove)
            {
                c.Paint -= DrawBorder;
                c.Paint += DrawBorder;
            }
            else
            {
                c.Paint -= DrawBorder;
            }
            c.Refresh();
        }

        private void DrawBorder(object sender, PaintEventArgs e)
        {
            base.OnPaint(e);
            ControlPaint.DrawBorder(e.Graphics, ClientRectangle,
                                         Color.Transparent, 1, ButtonBorderStyle.Solid,
                                         Color.Black, 1, ButtonBorderStyle.Solid,
                                         Color.Red, 1, ButtonBorderStyle.Dotted,
                                         Color.Red, 1, ButtonBorderStyle.Dotted);
        }

        public void ShowWindow(Size s, bool AutoSize)
        {
            if (AutoSize)
            {
                var th = textBox1.Size.Height;
                textBox1.Height = s.Height;
                if (s.Height > th)
                {
                    this.Size = new Size(s.Width, this.Size.Height + (s.Height - th) + 20);
                }
                else
                {
                    if ((th - s.Height) < this.Size.Height)
                    {
                        this.Size = new Size(s.Width, this.Size.Height - (th - s.Height));
                    }
                }
                textBox1.Height = s.Height;
            }
            else
            {
                this.Size = s;
            }

            Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, Width, Height, 20, 20));
            Rectangle workingArea = Screen.GetWorkingArea(this);
            this.Location = new Point(workingArea.Right - Size.Width,
                                      workingArea.Bottom - Size.Height);
            this.Show();
        }

        private void NotificationForm_Load(object sender, EventArgs e)
        {                        
            var t = new Timer();
            t.Interval = 1000 / 100;
            int i = 0;
            t.Tick += (arg1, arg2) =>
            {
                if (!this.IsDisposed)
                {
                    this.Opacity = ((double)i) / 100;
                    i++;
                    if (i >= 100)
                    {
                        this.Opacity = 1;
                        t.Stop();
                        t.Dispose();
                    }
                }               
            };
            t.Start();
        }

        public void Exit()
        {           
            var t = new Timer();
            t.Interval = 1000 / 100;
            int i = 98;
            t.Tick += (arg1, arg2) =>
            {
                this.Opacity = ((double)i) / 100;
                i--;
                if (i == 0)
                {
                    t.Stop();
                    t.Dispose();
                    this.Close();
                }
            };
            t.Start();
        }

    }

}

