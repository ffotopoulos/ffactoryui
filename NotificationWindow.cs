﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

///<summary>
///A basic notification window
/// </summary>


namespace FFactoryUI
{
    public partial class NotificationWindow : Component
    {
        private NotificationForm frm = new NotificationForm();
        public bool AutoSize { get; set; }
        public int Delay { get; set; }
        private bool disposed = false;
        public Size WindowSize { get; set; }

        public bool ShowHeaderBorder
        {
            get { return frm.ShowBorder; }
            set { frm.ShowBorder = value; }
        }

        public Color BackgroundColor
        {
            get { return frm.BackGroundColor; }
            set { frm.BackGroundColor = value; }
        }
        public Label Header
        {
            get { return frm.Header; }
        }
        public Font ContentFont
        {
            get { return frm.ContentFont; }
            set { frm.ContentFont = value; }
        }

        public Color ContentForeColor
        {
            get { return frm.ContentForeColor; }
            set { frm.ContentForeColor = value; }
        }

        public string ContentText
        {
            get { return frm.ContentText; }
            set
            {
                frm.ContentText = value;
            }
        }
        public Image CloseImage
        {
            get { return frm.CloseImage; }
            set { frm.CloseImage = value; }
        }
        public bool CloseImageVisible
        {
            get { return frm.CloseImageVisible; }
            set { frm.CloseImageVisible = value; }
        }

        public Image Image
        {
            get { return frm.Image; }
            set { frm.Image = value; }
        }
        public bool ImageVisible
        {
            get { return frm.ImageVisible; }
            set { frm.ImageVisible = value; }
        }

        public HorizontalAlignment ContentTextAlign
        {
            get { return frm.ContentTextAlign; }
            set { frm.ContentTextAlign = value; }
        }

        public NotificationWindow()
        {
            InitializeComponent();
            WindowSize = frm.Size;
            AutoSize = true;
            ShowHeaderBorder = true;
            Delay = 4000;
            frm.close.Click += (s, e) =>
            {
                disposed = true;
                frm.Close();
                this.Dispose();
            };
        }

        public NotificationWindow(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        public void Show()
        {
            if (!disposed)
            {
                if (!frm.Visible)
                {
                    if (AutoSize)
                    {
                        var finalSize = WindowSize;
                        var s = new Size(TextRenderer.MeasureText(ContentText, ContentFont).Width + 40, TextRenderer.MeasureText(ContentText, ContentFont).Height);
                        finalSize = new Size(finalSize.Width, s.Height);
                        if (s.Width > frm.MinimumSize.Width)
                            finalSize = new Size(s.Width, s.Height);
                        WindowSize = finalSize;
                    }
                    frm.ShowWindow(WindowSize, AutoSize);
                    System.Threading.Tasks.Task.Factory.StartNew(() =>
                    {
                        System.Threading.Thread.Sleep(Delay);
                        if (!disposed)
                        {
                            frm.Invoke(new MethodInvoker(() => frm.Exit())); //access ui thread;
                            disposed = true;
                            this.Dispose();
                        }
                    });
                }
            }
        }

    }
}
