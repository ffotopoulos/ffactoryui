﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace FFactoryUI
{
    /// <summary>
    /// Basic ( really basic) Drag and drop file handler by Fotis Fotopoulos 
    /// Works similarly to Dropzone.js
    /// ToDo: maaaaaaany things
    /// </summary>
    public partial class FFilesDrop : UserControl
    {
        #region properties
        private string helpText;
        private DataTable dtFiles = new DataTable();
        private Image helpRowImage;
        private Color dropZoneBGColor;
        private Color helpRowBackColor;
        private Color helpRowForeColor;
        private Color mainBGColor;
        private bool _useButtonToClearFiles;
        public bool _showButton;
        public bool _helpRowVisible;

        [
            Category("FFProperties")
        ]
        public bool ShowFilePathTooltipOnMouseOver
        {
            get { return lboxFiles.ShowCellToolTips; }
            set { lboxFiles.ShowCellToolTips = value; }
        }

        [
            Category("FFProperties")
        ]
        public bool EnableDragAndDrop
        {
            get { return lboxFiles.AllowDrop; }
            set { lboxFiles.AllowDrop = value; }
        }

        [
           Category("FFProperties")
        ]
        public bool HelpRowVisible
        {
            get { return this._helpRowVisible; }
            set
            {
                if (value)
                {
                    fixDataGridViewHelpRow(!value);
                }
                else
                {
                    fixDataGridViewHelpRow();
                }
                _helpRowVisible = value;
            }
        }

        [
            Category("FFProperties")
        ]
        public string HelpText
        {
            get { return this.helpText; }
            set
            {
                helpText = value;
                fixDataGridViewHelpRow();
                if (_helpRowVisible)
                    fixDataGridViewHelpRow(!_helpRowVisible);
            }
        }

        [
            Category("FFProperties")
        ]
        public Color HelpRowBackColor
        {
            get { return this.helpRowBackColor; }
            set
            {
                this.helpRowBackColor = value;
            }
        }

        [
           Category("FFProperties")
       ]
        public Color HelpRowForeColor
        {
            get { return this.helpRowForeColor; }
            set
            {
                this.helpRowForeColor = value;
            }
        }

        [
            Category("FFProperties")
        ]
        public Image HelpRowImage
        {
            get { return this.helpRowImage; }
            set
            {
                this.helpRowImage = value;
                fixDataGridViewHelpRow();
                if (_helpRowVisible)
                    fixDataGridViewHelpRow(!_helpRowVisible);
            }
        }

        [
            Category("FFProperties")
        ]
        public Color MainBackGroundColor
        {
            get { return mainBGColor; }
            set
            {
                mainBGColor = value;
                this.pnlMain.BackColor = value;
            }
        }

        [
            Category("FFProperties")
        ]
        public Color DropZoneBackGroundColor
        {
            get { return dropZoneBGColor; }
            set
            {
                dropZoneBGColor = value;
                this.lboxFiles.BackColor = value;
            }
        }

        [
            Category("FFProperties")
        ]
        public bool UseButtonToClearFiles
        {
            get { return _useButtonToClearFiles; }
            set
            {
                _useButtonToClearFiles = value;
                if (value)
                {
                    button.Click -= btnClick;
                    button.Click += btnClick;
                }
                else
                {
                    button.Click -= btnClick;
                }
            }
        }

        [
           Category("FFProperties")
        ]
        public string InitialDirectory { get; set; }


        [
          Category("FFProperties")
        ]
        public string FilterString { get; set; }

        [
          Category("FFProperties")
        ]
        public bool AllowMultipleFiles { get; set; }

        [
          Category("FFProperties")
        ]
        public bool ShowButton
        {
            get { return _showButton; }
            set
            {
                _showButton = value;
                button.Visible = value;
            }

        }
        [
          Category("FFProperties")
        ]
        public Color FileNamesColor
        {
            get { return lboxFiles.ForeColor; }
            set
            {
                lboxFiles.ForeColor = value;
            }
        }

        [
          Category("FFProperties")
        ]
        public Label FileCountLabel
        {
            get { return lblFileCount; }
        }


        [
          Category("FFProperties")
        ]
        public Button Button
        {
            get { return this.button; }
        }


        public int FileCount
        {
            get
            {
                if (dtFiles != null)
                {
                    return dtFiles.Rows.Cast<DataRow>()
                                    .Where(r => r["FilePath"].ToString() != "HelpRow")
                                    .Count();
                }
                return 0;
            }
        }

        #endregion


        ////init plus default properties values
        public FFilesDrop()
        {
            InitializeComponent();
            initProperties();
            initDataGridView();
        }

        private void initDataGridView()
        {
            this.lboxFiles.DataSource = dtFiles;

            this.lboxFiles.RowPrePaint += (s, e) =>
            {
                if (lboxFiles.Rows[e.RowIndex].Cells["FilePath"].Value.ToString() == "HelpRow")
                {
                    lboxFiles.Rows[e.RowIndex].DefaultCellStyle.BackColor
                        = this.helpRowBackColor;
                    lboxFiles.Rows[e.RowIndex].DefaultCellStyle.ForeColor
                        = this.helpRowForeColor;
                }
            };
        }

        private void initProperties()
        {
            dtFiles.Columns.Add("FileName");
            dtFiles.Columns.Add("FileType", typeof(Image));
            dtFiles.Columns.Add("FilePath");
            this.ShowFilePathTooltipOnMouseOver = true;
            this.EnableDragAndDrop = true;
            this._helpRowVisible = true;
            this.helpText = "Drop files or click here ";
            this.helpRowImage = Properties.Resources.add;
            this.helpRowBackColor = Color.FromArgb(240, 248, 255);
            this.AllowMultipleFiles = true;
            this.ShowButton = true;
            this.UseButtonToClearFiles = true;
            this.FilterString = "All files (*.*)|*.*";
            this.button.VisibleChanged += buttons_VisibleChanged;
            this.InitialDirectory = string.Empty;
            this.lblFileCount.Text = "Files:" + this.FileCount;
        }

        private void fixDataGridViewHelpRow(bool remove = true)
        {
            if (dtFiles != null && _helpRowVisible)
            {
                lboxFiles.DataSource = null;
                if (remove)
                {
                    foreach (DataRow row in dtFiles.Rows.Cast<DataRow>()
                                                .Where(r => r["FilePath"].ToString() == "HelpRow")
                                                .ToList())
                    {
                        dtFiles.Rows.Remove(row);
                    }
                }
                else
                {
                    var exists = dtFiles.Rows.Cast<DataRow>()
                                    .Where(r => r["FilePath"].ToString() == "HelpRow")
                                    .Any();
                    if (!exists)
                        dtFiles.Rows.Add(this.helpText, this.helpRowImage, "HelpRow");
                }
                bindDataToGrid(dtFiles);
            }
        }

        private void bindDataToGrid(DataTable t)
        {
            lboxFiles.DataSource = null;
            if (t != null && t.Rows.Count > 0)
            {
                lboxFiles.DataSource = t;
                lboxFiles.Columns["FilePath"].Visible = false;
                lblFileCount.Text = "Files:" + FileCount;
                lboxFiles.TabStop = false;
                lboxFiles.ClearSelection();
                lboxFiles.CancelEdit();
                if (lboxFiles.CurrentCell != null)
                    lboxFiles.CurrentCell.Selected = false;
            }
        }

        private void lboxFiles_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            if (!AllowMultipleFiles && files.Length > 1)
            {
                return;
            }
            List<string> filesList = new List<string>();

            foreach (var ff in files
                            .Where(f => File.Exists(f)))
            {
                filesList.Add(ff);
            }
            AddFiles(filesList);
        }

        private void lboxFiles_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop, false))
            {
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
                if (!AllowMultipleFiles)
                {
                    if (files.Length > 1)
                    {
                        e.Effect = DragDropEffects.None;
                    }
                    else
                    {
                        e.Effect = DragDropEffects.All;
                    }
                }
                else
                {
                    e.Effect = DragDropEffects.All;  
                }                
            }
        }

        public List<string> getFiles()
        {
            List<string> files = new List<string>();
            if (lboxFiles.DataSource != null)
            {
                DataTable Files = lboxFiles.DataSource as DataTable;
                foreach (DataRow r in Files.Rows.Cast<DataRow>()
                                        .Where(row => row["FilePath"].ToString() != "HelpRow")
                                        .ToList())
                {
                    files.Add(r["FilePath"].ToString());
                }
            }
            return files;
        }

        public void AddFile(string file)
        {
            if (!string.IsNullOrEmpty(file.Trim()) && File.Exists(file))
            {
                var ext = Path.GetExtension(file).Replace(".", "");
                if (ext == "docx")
                    ext = "doc";

                var img = Properties.Resources.ResourceManager.GetObject(ext);
                if (img == null)
                {
                    img = Properties.Resources._blank;
                }
                fixDataGridViewHelpRow();
                dtFiles.Rows.Add(Path.GetFileName(file), (Image)img, file);
                bindDataToGrid(dtFiles);
                fixDataGridViewHelpRow(false);
            }
        }


        public void AddFiles(List<string> files)
        {
            if (files != null && files.Count > 0)
            {
                foreach (var ff in files
                                    .Where(f => File.Exists(f)))
                {
                    AddFile(ff);
                }
            }
        }


        private void btnClick(object sender, EventArgs e)
        {
            RemoveAllFiles();
        }

        private void buttons_VisibleChanged(object sender, EventArgs e)
        {
            if (!button.Visible && !lblFileCount.Visible)
                lboxFiles.Dock = DockStyle.Fill;
            else if (button.Visible || lblFileCount.Visible)
                lboxFiles.Dock = DockStyle.None;
        }

        private void lboxFiles_CellMouseMove(object sender, DataGridViewCellMouseEventArgs e)
        {
            this.Cursor = Cursors.Default;
            if (e.RowIndex >= 0)
            {
                if (lboxFiles.DataSource != null && lboxFiles.Rows.Count > 0)
                {
                    if (lboxFiles.Rows[e.RowIndex].Cells["FilePath"].Value.ToString() == "HelpRow")
                    {
                        this.Cursor = Cursors.Hand;
                    }
                    else if (!string.IsNullOrEmpty(lboxFiles.Rows[e.RowIndex].Cells["FilePath"].Value.ToString().Trim()))
                    {
                        this.Cursor = Cursors.Help;
                    }
                }
            }

        }

        private void lboxFiles_SelectionChanged(object sender, EventArgs e)
        {
            lboxFiles.ClearSelection();
        }

        public void RemoveAllFiles()
        {
            foreach (DataRow row in dtFiles.Rows.Cast<DataRow>()
                                       .Where(r => r["FilePath"].ToString() != "HelpRow")
                                       .ToList())
            {
                row.Delete();
            }
            bindDataToGrid(dtFiles);
        }

        public void RemoveFile(string filePath)
        {
            try
            {
                var rowToDel = dtFiles.Rows.Cast<DataRow>()
                                      .Where(r => r["FilePath"].ToString() == filePath && filePath != "HelpRow")
                                      .FirstOrDefault();
                if (rowToDel != null)
                {
                    dtFiles.Rows.Remove(rowToDel);
                    bindDataToGrid(dtFiles);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveFilesWithName(string fileName)
        {
            try
            {
                var rowToDel = dtFiles.Rows.Cast<DataRow>()
                                      .Where(r => r["FileName"].ToString() == fileName
                                                && r["FilePath"].ToString() != "HelpRow")
                                      .FirstOrDefault();
                if (rowToDel != null)
                {
                    dtFiles.Rows.Remove(rowToDel);
                    bindDataToGrid(dtFiles);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void lboxFiles_MouseClick(object sender, MouseEventArgs e)
        {
            lboxFiles.ClearSelection();
            int row = lboxFiles.HitTest(e.X, e.Y).RowIndex;
            if (row < 0)
                return;

            var fPath = lboxFiles.Rows[row].Cells["FilePath"].Value.ToString();
            if (fPath != "HelpRow")
            {
                if (lboxFiles.CurrentCell != null)
                    lboxFiles.CurrentCell.Selected = false;

                ContextMenuStrip m = new ContextMenuStrip();
                var removeItem = new ToolStripMenuItem("Remove File");
                removeItem.Image = Properties.Resources.deleteIcon;
                removeItem.MouseHover += (s, arg) =>
                {
                    lboxFiles.Rows[row].DefaultCellStyle.BackColor = Color.FromArgb(240, 128, 128);
                };
                removeItem.MouseLeave += (s, arg) =>
                {
                    lboxFiles.Rows[row].DefaultCellStyle.BackColor = Color.White;
                };
                removeItem.Click += (s, arg) =>
                    {
                        RemoveFile(fPath);
                    };
                var openFileDirectoryItem = new ToolStripMenuItem("Open File Directory");
                openFileDirectoryItem.Image = Properties.Resources._blank;
                openFileDirectoryItem.Click += (s, arg) =>
                {
                    openFileDirectory(fPath);
                };
                m.Items.Add(openFileDirectoryItem);
                m.Items.Add(removeItem);                
                m.Show(lboxFiles, new Point(e.X, e.Y));
            }
            else
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                var initDir = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                if (InitialDirectory != string.Empty)
                    initDir = InitialDirectory;
                openFileDialog.InitialDirectory = initDir;
                openFileDialog.Filter = FilterString;
                openFileDialog.FilterIndex = 1;
                openFileDialog.RestoreDirectory = true;
                openFileDialog.Multiselect = AllowMultipleFiles;
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    List<string> files = new List<string>();
                    foreach (var ff in openFileDialog.FileNames
                                            .Where(f => File.Exists(f)))
                    {
                        files.Add(ff);
                    }
                    AddFiles(files);
                }
            }

        }

        private void openFileDirectory(string filePath)
        {
            try
            {                
                Process.Start("explorer.exe", Path.GetDirectoryName(filePath));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void lboxFiles_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (lboxFiles.Rows[e.RowIndex].Cells["FilePath"].Value.ToString() != "HelpRow")
            {
                var cell = lboxFiles.Rows[e.RowIndex].Cells[e.ColumnIndex];
                cell.ToolTipText = lboxFiles.Rows[e.RowIndex].Cells["FilePath"].Value.ToString();
            }

        }
    }
}
